public class Arrays {
 
    public static void main(String[] args) {
    	double arr[] = new double[]{10.0, 4.0, 1.0, 2.0};
        
        for (int  i = 0; i < arr.length; i++) {
        	arr[i] =arr[i] * 1.1;
        }
        
        for (int i = arr.length-1; i > 0; i--) {
        	for (int j = 0; j < i; j++) {
        		if (arr[j] < arr[j + 1]) {
        			double a = arr[j];
        			arr[j] = arr[j + 1];
        			arr[j + 1] = a;
                    }
                }
            }
        for (int i = 0; i < arr.length; i++) {
        	System.out.print(arr[i]+" ");
        }
}
}